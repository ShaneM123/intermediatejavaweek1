
public class Temp{
	private double fahrenheit;
	private double celsius;
	private double kelvin;

	public boolean celsius(double celsius){
	    if(celsius<-273.15){
	        return false;
	    }
	 this.celsius=celsius;
	 this.fahrenheit= celsius * 9/5 + 32;
	 this.kelvin= celsius + 273.15;
	 return true;
 }
 
	 public boolean kelvin(double kelvin){
 	if(kelvin<0){
 		return false;
	}
		 this.kelvin=kelvin;
			 	 this.fahrenheit= kelvin * 9/5 - 459.67;
	             this.celsius= kelvin - 273.15;
	             return true;
 }
 
	 public boolean fahrenheit(double fahrenheit){
	     if(fahrenheit<-459.67){
	         return false;
         }
		 this.fahrenheit=fahrenheit;
			 this.celsius= (fahrenheit - 32) * 5/9;
			 this.kelvin= (fahrenheit + 459.67)* 5/9;
			 return true;
 }


 public double getCelsius(){
	 return this.celsius;
 }
 public double getFahrenheit(){
	 return this.fahrenheit;
 }
 public double getKelvin(){
	 return this.kelvin;
 }
 
}