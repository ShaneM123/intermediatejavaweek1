public class Weight{
	private double lbs;
	private double kgs;

    public double getLbs() {
        return this.lbs;
    }

    public void lbs(double lbs) {
        this.lbs = lbs;
        this.kgs= lbs*0.45359237;
    }

    public double getKgs() {
        return this.kgs;
    }

    public void kgs(double kgs) {
        this.kgs = kgs;
        this.lbs= kgs*2.2046226218;
    }
}

