 public class Height{
     private double inches;
     private double cms;

     public void inches(double inches){
         this.inches=inches;
         this.cms= inches*2.54;

     }

     public void cms(double cms){
         this.cms=cms;
         this.inches=cms*0.393701;
     }

     public double getInches() {
         return inches;
     }

     public double getCms() {
         return cms;
     }
 }